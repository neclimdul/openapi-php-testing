<?php

namespace Neclimdul\OpenapiPhpTesting;

use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\PropertyNormalizer;
use Symfony\Component\Serializer\Serializer;

trait FakeValuesTrait
{
    private Generator $faker;

    protected function setFaker(Generator $faker): void
    {
        $this->faker = $faker;
    }

    /**
     * Provide fake request values.
     *
     * @param int $code
     *   Response code.
     * @param string[] $param_types
     *   Parameter type strings.
     * @param string|null $return_type
     *   Return type string. Null if no return.
     * @param string[] $content_types
     *   List of content types returned by the API.
     * @return \Generator<array{int, string[], string, string[]>
     *   Generator with the valid responses.
     */
    protected static function provideFakeRequests(
        int $code,
        array $param_types,
        ?string $return_type,
        array $content_types
    ): \Generator {
        // Always test something.
        if (empty($content_types)) {
            $content_types = [''];
        }
        $faker = Factory::create();
        foreach ($content_types as $content_type) {
            // Skip for now because XML is hard and needs a lot of work before
            // we really have an MVP.
            if ($content_type == 'application/xml') {
                continue;
            }
            $params = [];
            foreach ($param_types as $param_type) {
                $params[] = self::createFakeValue($faker, $param_type, null);
            }
            yield "$code $content_type" => [
                $code,
                $params,
                $return_type ?
                    static::encodeFakeResponse(
                        self::createFakeValue($faker, $return_type, null),
                        $content_type
                    ) :
                    '',
                isset($return_type),
                $content_type,
                // Raw args for debugging and hacking purposes.
                func_get_args()
            ];
        }
    }

    /**
     * Encode a return value based on the content type.
     *
     * @param mixed $value
     *   The raw response data.
     * @param string $content_type
     *   The content type to encode to.
     * @return string|mixed
     *   The encoded value.
     */
    private static function encodeFakeResponse(mixed $value, string $content_type): mixed
    {
        return match ($content_type) {
            // Abuse symfony to do simple encoding/serialization.
            'application/xml' => (new Serializer([new PropertyNormalizer()], [new XmlEncoder()]))
                ->encode(json_decode(json_encode($value)), 'xml'),
            // Assume JSON is good enough for everything else.
            default => json_encode($value, JSON_PRETTY_PRINT),
        };
    }


    /**
     * Generate a fake value for validation.
     *
     * @param \Faker\Generator $faker
     *   Faker generator used to generate values.
     * @param string $type
     *   A php type or a valid doc type.
     * @param scalar[]|null $values
     *   A list of values for enumerated types. If null any value is assumed valid.
     * @return mixed
     */
    private static function createFakeValue(Generator $faker, string $type, ?array $values): mixed
    {
        if (strcasecmp(substr($type, -2), '[]') === 0) {
            $subType = substr($type, 0, -2);
            return [self::createFakeValue($faker, $subType, $values)];
        } elseif (preg_match('/array<(.*),(.*)>/', $type, $matches)) {
            [, $key_type, $subtype] = $matches;
            return [
                self::createFakeValue($faker, $key_type, null) => self::createFakeValue($faker, $subtype, null),
            ];
        }
        // We can't know the interface namespace beforehand so if the method
        // exist we assume it's correct.
        if (class_exists($type) && method_exists($type, 'openAPITypes')) {
            $model = new $type();
            $types = $type::openAPITypes();
            $getters = $type::getters();
            $setters = $type::setters();
            foreach ($model->listInvalidProperties() as $field => $reason) {
                $setter = $setters[$field];
                $values = $getters[$field] . 'AllowableValues';
                $sub_values = method_exists($model, $values) ? $model->$values() : null;
                $model->$setter(self::createFakeValue($faker, $types[$field], $sub_values));
            }
            return $model;
        }

        if (!isset($values)) {
            // If complex type, split so we can match the base type.
            $extra = null;
            if (str_contains($type, ':')) {
                [$type, $extra] = explode(':', $type, 2);
            }
            $values = match ($type) {
                'string' => [$faker->word()],
                // https://github.com/OpenAPITools/openapi-generator/issues/13335
                'double', 'float' => [self::convertNumber($faker, $extra, true)],
                'int' => [self::convertNumber($faker, $extra, false)],
                'bool' => [$faker->boolean()],
                '\DateTime', \DateTime::class => [$faker->dateTimeAD()],
                'object' =>  [new \stdClass()],
                '\SplFileObject', \SplFileObject::class => [new \SplFileObject(__FILE__)],
                default => self::markTestSkipped("This type is not mocked yet: '$type'"),
            };
        }
        return array_pop($values);
    }

    /**
     * Generate a fake value for validation.
     *
     * @param string $type
     *   A php type or a valid doc type.
     * @param scalar[]|null $values
     *   A list of values for enumerated types. If null any value is assumed valid.
     * @return mixed
     */
    protected function getFakeValue(string $type, ?array $values): mixed
    {
        return self::createFakeValue($this->faker, $type, $values);
    }

    /**
     * Convert complex number into a value.
     *
     * @param \Faker\Generator $faker
     *   Faker generator
     * @param string|null $extra
     *   Extra constraints.
     * @param bool $is_float
     *   True if value should be a float.
     * @return int|float
     */
    private static function convertNumber(
        Generator $faker,
        ?string $extra,
        bool $is_float
    ): int|float {
        $min = $max = null;
        if (!empty($extra)) {
            if (str_contains($extra, ',')) {
                [$min, $max] = explode(',', $extra, 2);
            } elseif (str_starts_with($extra, '[') || str_starts_with($extra, '(')) {
                $min = $extra;
            } else {
                $max = $extra;
            }
        }

        if (!isset($min) && !isset($max)) {
            if ($is_float) {
                return $faker->randomFloat();
            } else {
                return $faker->randomNumber();
            }
        }
        if (isset($min)) {
            $min_inc = str_starts_with($min, '[');
            $min = ltrim($min, '([');
            $min_valid = fn(int | float $value): bool => (!$min_inc || ($value >= $min))
                && ($min_inc || ($value > $min));
        } else {
            $min_valid = fn() => true;
        }
        if (isset($max)) {
            $max_inc = str_ends_with($max, ']');
            $max = rtrim($max, '])');
            $max_valid = fn(int | float $value): bool => (!$max_inc || ($value <= $max))
                && ($max_inc || ($value < $max));
        } else {
            $max_valid = fn() => true;
        }
        $isValid = fn($value) => $min_valid($value) && $max_valid($value);
        do {
            if (isset($min) && isset($max)) {
                if ($is_float) {
                    $value = $faker->randomFloat(min: $min, max: $max);
                } else {
                    $value = $faker->numberBetween($min, $max);
                }
            } elseif (isset($min)) {
                if ($is_float) {
                    $value = $faker->randomFloat(min: $min);
                } else {
                    $value = $faker->numberBetween($min);
                }
            } else {
                if ($is_float) {
                    $value = $faker->randomFloat(max: $max);
                } else {
                    $value = $faker->numberBetween(int2: $max);
                }
            }
        } while (!$isValid($value));
        return $value;
    }
}
