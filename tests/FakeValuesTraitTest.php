<?php

namespace Neclimdul\OpenapiPhpTesting\Tests;

use Faker\Factory;
use Neclimdul\OpenapiPhpTesting\FakeValuesTrait;
use Neclimdul\OpenapiPhpTesting\Tests\Fixtures\BasicModel;
use NecLimDul\PhpUnitExceptions\ExceptionAssertionTrait;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhpTesting\FakeValuesTrait
 */
class FakeValuesTraitTest extends TestCase
{
    use ExceptionAssertionTrait;

    private $sot;

    public function setUp(): void
    {
        parent::setUp();
        $this->sot = new class () {
            use FakeValuesTrait {
                getFakeValue as public;
            }

            public function __construct()
            {
                $this->setFaker(Factory::create());
            }

            public static function markTestSkipped(): never
            {
                throw new \InvalidArgumentException();
            }

            public function getFakeRequests(
                int $code,
                array $param_types,
                ?string $return_type,
                array $content_types
            ): array {
                return iterator_to_array(
                    static::provideFakeRequests($code, $param_types, $return_type, $content_types)
                );
            }
        };
    }

    public static function provideRequestParams()
    {
        //'application/xml',
        return [
            [
                200,
                ['int', 'string', 'int[]'],
                'string',
                ['', 'application/json'],
            ],
        ];
    }

    /**
     * @dataProvider provideRequestParams
     */
    public function testProvideFakeRequests(
        int $code,
        array $param_types,
        ?string $return_type,
        array $content_types
    ): void {
        $t = $this->sot->getFakeRequests(
            $code,
            $param_types,
            $return_type,
            $content_types
        );
        $this->assertSameSize($content_types, $t);
        foreach ($t as $response) {
            $this->assertEquals($code, $response[0]);
            $this->assertSameSize($param_types, $response[1]);
        }
    }

    /**
     * @dataProvider provideRequestParams
     */
    public function testProvideFakeRequestsEdge(): void
    {
        // Too hard.
        $this->assertEmpty($this->sot->getFakeRequests(200, [], '', ['application/xml']));
        $this->assertCount(1, $this->sot->getFakeRequests(200, [], '', []));
        //
    }

    public function testGetFakePrimitives(): void
    {
        $this->assertIsString($this->sot->getFakeValue('string', null));
        $this->assertIsInt($this->sot->getFakeValue('int', null));
        $this->assertIsFloat($this->sot->getFakeValue('float', null));
        $this->assertIsFloat($this->sot->getFakeValue('double', null));
        $this->assertIsBool($this->sot->getFakeValue('bool', null));
        $this->assertInstanceOf(\DateTimeInterface::class, $this->sot->getFakeValue('\DateTime', null));
        $this->assertInstanceOf(\DateTimeInterface::class, $this->sot->getFakeValue(\DateTime::class, null));
        $this->assertIsObject($this->sot->getFakeValue('object', null));
        $this->assertInstanceOf(\SplFileObject::class, $this->sot->getFakeValue(\SplFileObject::class, null));
        $this->assertInstanceOf(\SplFileObject::class, $this->sot->getFakeValue('\SplFileObject', null));
        for ($i = 0; $i < 10; $i++) {
            $value = $this->sot->getFakeValue('int:[1,5]', null);
            $this->assertGreaterThanOrEqual(1, $value);
            $this->assertLessThanOrEqual(5, $value);
            $value = $this->sot->getFakeValue('int:[15', null);
            $this->assertGreaterThanOrEqual(15, $value);
            $value = $this->sot->getFakeValue('int:15]', null);
            $this->assertLessThanOrEqual(15, $value);
            $value = $this->sot->getFakeValue('int:(1,5)', null);
            $this->assertGreaterThan(1, $value);
            $this->assertLessThan(5, $value);
            $value = $this->sot->getFakeValue('int:(15', null);
            $this->assertGreaterThan(15, $value);
            $value = $this->sot->getFakeValue('int:15)', null);
            $this->assertLessThan(15, $value);
            $value = $this->sot->getFakeValue('float:[1,5]', null);
            $this->assertGreaterThanOrEqual(1, $value);
            $this->assertLessThanOrEqual(5, $value);
            $value = $this->sot->getFakeValue('float:[15', null);
            $this->assertGreaterThanOrEqual(15, $value);
            $value = $this->sot->getFakeValue('float:15]', null);
            $this->assertLessThanOrEqual(15, $value);
            $value = $this->sot->getFakeValue('float:(1,5)', null);
            $this->assertGreaterThan(1, $value);
            $this->assertLessThan(5, $value);
            $value = $this->sot->getFakeValue('float:(15', null);
            $this->assertGreaterThan(15, $value);
            $value = $this->sot->getFakeValue('float:15)', null);
            $this->assertLessThan(15, $value);
        }
        // Unknown type skipped.
        $self = $this;
        $this->assertThrows(
            function () use ($self) {
                $self->sot->getFakeValue('notarealtype', null);
            },
            \InvalidArgumentException::class
        );
    }

    public function testGetFakeValueCollections(): void
    {
        $collection = $this->sot->getFakeValue('string[]', null);
        $this->assertIsArray($collection);
        $this->assertNotCount(0, $collection);
        foreach ($collection as $v) {
            $this->assertIsString($v);
        }

        $collection = $this->sot->getFakeValue('int[]', null);
        $this->assertIsArray($collection);
        $this->assertNotCount(0, $collection);
        foreach ($collection as $v) {
            $this->assertIsInt($v);
        }

        $collection = $this->sot->getFakeValue('array<string,string>', null);
        $this->assertIsArray($collection);
        $this->assertNotCount(0, $collection);
        foreach ($collection as $k => $v) {
            $this->assertIsString($k);
            $this->assertIsString($v);
        }
        $collection = $this->sot->getFakeValue('array<int,string>', null);
        $this->assertIsArray($collection);
        $this->assertNotCount(0, $collection);
        foreach ($collection as $k => $v) {
            $this->assertIsInt($k);
            $this->assertIsString($v);
        }
        $collection = $this->sot->getFakeValue('array<string,int>', null);
        $this->assertIsArray($collection);
        $this->assertNotCount(0, $collection);
        foreach ($collection as $k => $v) {
            $this->assertIsString($k);
            $this->assertIsInt($v);
        }
        $collection = $this->sot->getFakeValue('array<int,int>', null);
        $this->assertIsArray($collection);
        $this->assertNotCount(0, $collection);
        foreach ($collection as $k => $v) {
            $this->assertIsInt($k);
            $this->assertIsInt($v);
        }
    }

    public function testBasicModel(): void
    {
        $model = $this->sot->getFakeValue(BasicModel::class, null);
        $this->assertIsString($model->getType1());
        $this->assertIsInt($model->getType2());
        $this->assertContains($model->getType2(), $model->getType2AllowableValues());
    }
}
