<?php

namespace Neclimdul\OpenapiPhpTesting\Tests\Fixtures;

class BasicModel
{
    private $container = [];

    public static function openAPITypes(): array
    {
        return [
            'type1' => 'string',
            'type2' => 'int',
        ];
    }

    public static function getters(): array
    {
        return [
            'type1' => 'getType1',
            'type2' => 'getType2',
        ];
    }

    public static function setters(): array
    {
        return [
            'type1' => 'setType1',
            'type2' => 'setType2',
        ];
    }

    public function listInvalidProperties(): array
    {
        return ['type1' => '', 'type2' => ''];
    }

    public function getType1(): string
    {
        return $this->container['type1'];
    }

    public function setType1(string $value): void
    {
        $this->container['type1'] = $value;
    }

    public function getType2AllowableValues(): array
    {
        return [
            1,
            2,
            10001,
        ];
    }

    public function getType2(): int
    {
        return $this->container['type2'];
    }

    public function setType2(int $value): void
    {
        $this->container['type2'] = $value;
    }
}
