<?php

namespace Neclimdul\OpenapiPhpTesting\Tests;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use Neclimdul\OpenapiPhp\Helper\ApiExceptionInterface;
use Neclimdul\OpenapiPhp\Helper\Model\ModelInterface;
use Neclimdul\OpenapiPhp\Helper\ObjectSerializer;
use Neclimdul\OpenapiPhpTesting\ApiAssertionsTrait;
use Neclimdul\OpenapiPhpTesting\Tests\Fixtures\ApiException;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Psr\Http\Message\ResponseInterface;

/**
 * @coversDefaultClass \Neclimdul\OpenapiPhpTesting\ApiAssertionsTrait
 */
class ApiAssertionsTraitTest extends TestCase
{
    use ApiAssertionsTrait;
    use ProphecyTrait;

    private MockHandler $mockHandler;

    /**
     * @var \GuzzleHttp\Client
     */
    private GuzzleClient $client;

    private TestCase $sot;

    /**
     * @var \Neclimdul\OpenapiPhp\Helper\ObjectSerializer<ApiExceptionInterface, ModelInterface>
     */
    private ObjectSerializer $serializer;

    public function setUp(): void
    {
        parent::setUp();
        $this->mockHandler = new MockHandler();
        $this->client = new GuzzleClient(['handler' => HandlerStack::create($this->mockHandler)]);
        $this->setAssertedApiException(ApiException::class);

        $this->serializer = new ObjectSerializer(ModelInterface::class, ApiExceptionInterface::class);
    }

    public static function provideRequestData()
    {
        return [
            [123, '{}', 'object', 'application/json'],
            [400, '{}', 'object', 'application/json'],
            [500, '{}', 'object', 'application/json'],
            [200, '{"foo":"bar"}', 'object', 'application/json'],
            [
                200,
                new \SplFileObject(realpath(__DIR__ . '/../README.md'), 'r+'),
                \SplFileObject::class,
                'text/markdown'
            ],
            [201, '""', 'string', '*/*'],
        ];
    }

    /**
     * @covers ::assertRequest
     * @covers ::makeRequest
     * @covers ::assertResponse
     * @covers ::assertResponse
     * @dataProvider provideRequestData
     */
    public function testAssertRequest($code, $data, $type, $produces)
    {
        $f = function () use ($type) {
            try {
                $response = $this->client
                    ->send(new Request('GET', '/foo'), []);
            } catch (RequestException $e) {
                throw new $this->exceptionClass(previous: $e);
            }
            return $this->processResponse(
                $response,
                $type
            );
        };
        $this->assertRequest($f, $data, $code, true, $produces);
    }

    /**
     * @covers ::assertRequestWithHttpInfo
     * @covers ::makeRequest
     * @covers ::assertResponse
     * @dataProvider provideRequestData
     */
    public function testAssertRequestWithHttpInfo($code, $data, $type, $produces)
    {
        $f = function () use ($type, $code, $produces) {
            try {
                $response = $this->client
                    ->send(new Request('GET', '/foo'), []);
            } catch (RequestException $e) {
                throw new $this->exceptionClass(previous: $e);
            }
            return [
                $this->processResponse(
                    $response,
                    $type
                ),
                $code,
                ['Content-Type' => [$produces]],
            ];
        };
        $this->assertRequestWithHttpInfo($f, $data, $code, true, $produces);
    }

    /**
     * @covers ::assertAsync
     * @covers ::makeAsyncRequest
     * @covers ::assertResponse
     * @dataProvider provideRequestData
     */
    public function testAssertAsync($code, $data, $type, $produces)
    {
        $f = fn() => $this->client
            ->sendAsync(new Request('GET', '/foo'))
            ->then(
                fn(ResponseInterface $r) => $this->processResponse($r, $type),
                function (RequestException $exception) {
                    throw new $this->exceptionClass(previous: $exception);
                }
            );
        $this->assertAsync($f, $data, $code, true, $produces);
    }

    /**
     * @covers ::assertAsyncWithHttpInfo
     * @covers ::makeAsyncRequest
     * @covers ::assertResponse
     * @dataProvider provideRequestData
     */
    public function testAssertAsyncWithHttpInfo($code, $data, $type, $produces)
    {
        // Assume guzzle would normally fulfill this.
        $f = fn() => $this->client
            ->sendAsync(new Request('GET', '/foo'))
            ->then(fn(ResponseInterface $r) => [
                $this->processResponse($r, $type),
                $r->getStatusCode(),
                $r->getHeaders(),
            ], function (RequestException $exception) {
                throw new $this->exceptionClass(previous: $exception);
            });
        $this->assertAsyncWithHttpInfo($f, $data, $code, true, $produces);
    }

    private function processResponse(ResponseInterface $response, $type)
    {
        return $this->serializer->deserialize(
            $response->getBody(),
            $type,
            $response->getHeaders()
        );
    }
}
