# OpenAPI PHP Templates
PHP OpenAPI testing helpers.

This project aims to consolidate shared logic for PHP OpenAPI projects. This
allows for deeper testing with limited additional code.

## Goals
This project shares its goals with its sibling template project it is designed
to support.

  - Faster development
  - Bug fixes
  - Additional spec compatibility
  - Testing

## Usage
Generally this package will be used with the templates it is designed to
support and not on its own. If you need to add it to your project you can do
so via composer.

```shell
composer require --dev neclimdul/openapi-php-testing
```

## License

This project is released under the [MIT license](LICENSE).
