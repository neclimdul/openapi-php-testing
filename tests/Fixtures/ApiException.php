<?php

namespace Neclimdul\OpenapiPhpTesting\Tests\Fixtures;

use Neclimdul\OpenapiPhp\Helper\ApiExceptionTypedBase;
use Neclimdul\OpenapiPhp\Helper\ApiExceptionInterface;

class ApiException extends ApiExceptionTypedBase implements ApiExceptionInterface
{
}
