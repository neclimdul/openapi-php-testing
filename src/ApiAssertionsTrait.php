<?php

namespace Neclimdul\OpenapiPhpTesting;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use GuzzleHttp\Psr7\Utils;
use Neclimdul\OpenapiPhp\Helper\Response\ApiResponseInterface;
use NecLimDul\PhpUnitExceptions\ExceptionAssertionTrait;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\PropertyNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @property \GuzzleHttp\Client $client
 */
trait ApiAssertionsTrait
{
    use ExceptionAssertionTrait;

    /**
     * @var class-string
     */
    private string $exceptionClass = '';

    /**
     * @param class-string $class
     */
    protected function setAssertedApiException(string $class): void
    {
        $this->exceptionClass = $class;
    }

    /**
     * Make a simple request and assert the behavior.
     *
     * @param callable $c
     *   Callable that runs the request handling code.
     * @param string|\SplFileObject $data
     *   The expected response data.
     * @param int $code
     *   The response code.
     * @param bool $rt
     *   Whether the response returns data.
     */
    protected function assertApiRequest(
        callable $c,
        string|\SplFileObject $data,
        int $code,
        bool $rt,
        string $produces = 'application/json'
    ): void {
        $response = $this->makeRequest($c, $data, $code, $produces, false);
        $this->assertApiResponse($data, $response, $rt, $produces);
    }

    /**
     * Make a simple request and assert the behavior.
     *
     * @param callable $c
     *   Callable that runs the request handling code.
     * @param string|\SplFileObject $data
     *   The expected response data.
     * @param int $code
     *   The response code.
     * @param bool $rt
     *   Whether the response returns data.
     */
    protected function assertRequest(
        callable $c,
        string|\SplFileObject $data,
        int $code,
        bool $rt,
        string $produces = 'application/json'
    ): void {
        if ($code >= 400) {
            if (empty($this->exceptionClass)) {
                $this->fail('Set the expected exception so we can test errors');
            }
            $self = $this;
            $this->assertThrows(
                function () use ($self, $c, $data, $code, $produces) {
                    $self->makeRequest($c, $data, $code, $produces);
                },
                $this->exceptionClass
            );
        } else {
            $response = $this->makeRequest($c, $data, $code, $produces);
            $this->assertResponse($data, $response, $rt, $produces);
        }
    }

    /**
     * Make a WithHttpInfo request and assert the behavior.
     *
     * @param callable $c
     *   Callable that runs the request handling code.
     * @param string|\SplFileObject $data
     *   The expected response data.
     * @param int $code
     *   The response code.
     * @param bool $rt
     *   Whether the response returns data.
     */
    protected function assertRequestWithHttpInfo(
        callable $c,
        string|\SplFileObject $data,
        int $code,
        bool $rt,
        string $produces = 'application/json'
    ): void {
        if ($code >= 400) {
            $self = $this;
            $this->assertThrows(
                function () use ($self, $c, $data, $code, $produces) {
                    $self->makeRequest($c, $data, $code, $produces);
                },
                $this->exceptionClass
            );
        } else {
            $result = $this->makeRequest($c, $data, $code, $produces);
            $this->assertResponse($data, $result[0], $rt, $produces);
            $this->assertEquals($code, $result[1]);
            $this->assertEquals(['Content-Type' => [$produces]], $result[2]);
        }
    }

    /**
     * Man an Async request and assert the behavior.
     *
     * @param callable $c
     *   Callable that runs the request handling code.
     * @param string|\SplFileObject $data
     *   The expected response data.
     * @param int $code
     *   The response code.
     * @param bool $rt
     *   Whether the response returns data.
     */
    protected function assertAsync(
        callable $c,
        string|\SplFileObject $data,
        int $code,
        bool $rt,
        string $produces = 'application/json'
    ): void {
        if ($code >= 400) {
            $self = $this;
            $this->assertThrows(
                function () use ($self, $c, $data, $code, $produces) {
                    $self->makeAsyncRequest($c, $data, $code, $produces);
                },
                $this->exceptionClass
            );
        } else {
            $result = $this->makeAsyncRequest($c, $data, $code, $produces);
            $this->assertResponse($data, $result, $rt, $produces);
        }
    }

    /**
     * Make an AsyncWithHttpInfo request and assert the behavior.
     *
     * @param callable $c
     *   Callable that runs the request handling code.
     * @param string|\SplFileObject $data
     *   The expected response data.
     * @param int $code
     *   The response code.
     * @param bool $rt
     *   Whether the response returns data.
     */
    protected function assertAsyncWithHttpInfo(
        callable $c,
        string|\SplFileObject $data,
        int $code,
        bool $rt,
        string $produces = 'application/json'
    ): void {
        if ($code >= 400) {
            $self = $this;
            $this->assertThrows(
                function () use ($self, $c, $data, $code, $produces) {
                    $self->makeAsyncRequest($c, $data, $code, $produces);
                },
                $this->exceptionClass
            );
        } else {
            $result = $this->makeAsyncRequest($c, $data, $code, $produces);
            $this->assertResponse($data, $result[0], $rt, $produces);
            $this->assertEquals($code, $result[1]);
            $this->assertEquals(['Content-Type' => [$produces]], $result[2]);
        }
    }

    /**
     * Make a simple guzzle request.
     *
     * @param callable $c
     *   Callable that runs the request handling code.
     * @param string|\SplFileObject $data
     *   The expected response data.
     * @param int $code
     *   The response code.
     * @param string $produces
     *   The type of content to produce. aka the Content-Type header.
     *
     * @return mixed
     *   What ever the request handler returned.
     */
    private function makeRequest(
        callable $c,
        string|\SplFileObject $data,
        int $code,
        string $produces,
        bool $legacy = true,
    ): mixed {
        if ($data instanceof \SplFileObject) {
            // Hack around Guzzle's semi broken handling of SplFileObject
            // @see https://github.com/guzzle/psr7/issues/256
            // We also use memory to work around problems with phpunit clobbering.
            //$s = new Stream(Utils::tryFopen('php://temp', 'r+'));
            $data->rewind();
            $s = new Stream(Utils::tryFopen('php://temp', 'r+'));
            $s->write($data->fread($data->getSize()));
            $data = $s;
        }
        $response = new Response($code, ['Content-Type' => [$produces]], $data);
        if ($legacy && $code >= 400) {
            $this->mockHandler->append(
                RequestException::create(new Request('GET', '/'), $response)
            );
        } else {
            $this->mockHandler->append(
                $response
            );
        }
        return $c();
    }

    /**
     * Make an async guzzle request.
     *
     * @param callable $c
     *   Callable that runs the request handling code.
     * @param string|\SplFileObject $data
     *   The expected response data.
     * @param int $code
     *   The response code.
     * @param string $produces
     *   The type of content to produce. aka the Content-Type header.
     *
     * @return mixed
     *   What ever the request handler returned.
     */
    protected function makeAsyncRequest(callable $c, string|\SplFileObject $data, int $code, string $produces): mixed
    {
        if ($data instanceof \SplFileObject) {
            // Hack around Guzzle's semi broken handling of SplFileObject
            // @see https://github.com/guzzle/psr7/issues/256
            $f = fopen('php://temp', 'rw');
            $data->rewind();
            fwrite($f, $data->fread($data->getSize()));
            $data = new Stream($f);
        }
        $response = new Response($code, ['Content-Type' => [$produces]], $data);
        if ($code >= 400) {
            $response = RequestException::create(
                new Request('GET', '/'),
                $response,
            );
        }
        $this->mockHandler->append($response);
        $c()
            ->then(
            /**
             * @param mixed $r
             * @return mixed
             */
                function (mixed $r) use (&$result) {
                    return $result = $r;
                }
            )
            ->wait();
        return $result;
    }

    /**
     * Assert a response from a request.
     *
     * @param mixed $data
     *   The expected raw data.
     * @param ApiResponseInterface $apiResponse
     *   The API response wrapper.
     * @param boolean $rt
     *   If the request has a return.
     * @param string $produces
     *   The content type expected to be produced. Also, the encoding of $data.
     */
    private function assertApiResponse(mixed $data, ApiResponseInterface $apiResponse, bool $rt, string $produces): void
    {
        // TODO more and better assertions.
        $this->assertResponse($data, $apiResponse->getData(), $rt, $produces);
    }

    /**
     * Assert a response from a request.
     *
     * @param mixed $data
     *   The expected raw data.
     * @param mixed $response
     *   The deserialized response.
     * @param boolean $rt
     *   If the request has a return.
     * @param string $produces
     *   The content type expected to be produced. Also, the encoding of $data.
     */
    private function assertResponse(mixed $data, mixed $response, bool $rt, string $produces): void
    {
        if ($rt) {
            if ($produces == 'application/xml') {
                // Abuse symfony to do simple encoding/serialization.
                $this->assertEquals(
                    $data,
                    (new Serializer([new PropertyNormalizer()], [new XmlEncoder()]))
                        ->encode($response, 'xml')
                );
            } elseif ($data instanceof \SplFileObject) {
                $this->assertInstanceOf(\SplFileObject::class, $response);
                $data->rewind();
                $response->rewind();
                $this->assertEquals($data->fread($data->getSize()), $response->fread($data->getSize()));
            } else {
                // Assume its JSON and hope.
                // @todo assert type.
                $this->assertJsonStringEqualsJsonString(
                    $data,
                    json_encode($response)
                );
            }
        } else {
            $this->assertNull($response);
        }
    }
}
